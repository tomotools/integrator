# Integrator

This project is the first step for support of XRD-CT in Nabu.
For now, it primarily aims at distributing ID15 `intrange` code for azimuthal integration.

## Installation

```bash
pip install git+https://gitlab.esrf.fr/tomotools/integrator
```

## Usage

First create a configuration file and fill-in the relevant parts:
```bash
integrator-config # creates a file "integrator.conf" which comes with pre-filled values
```
Then use `integrate-slurm` with this configuration file:

```bash
integrate-slurm integrator.conf
```

Alternatively, you can first spawn a set of "workers" (living on SLURM compute nodes), and then submit integration tasks to them. This avoids re-allocating SLURM resources each time.

```bash
integrator-spawn-cluster integrator.conf # the configuration file describes the workers setup
# in another terminal, connect to the newly created cluster
integrate-slurm --cluster "tcp://160.103.237.67:36087" integrator_dataset1.conf
integrate-slurm --cluster "tcp://160.103.237.67:36087" integrator_dataset2.conf
```

## Warning: work in progress

This repository is work in progress. This means that the API and command line tools are far from being stable.
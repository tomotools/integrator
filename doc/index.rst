.. Integrator documentation master file, created by
   sphinx-quickstart on Mon Aug 23 15:31:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Integrator |version|
====================

Distributed PyFAI processing


Getting started
----------------

.. toctree::
   :maxdepth: 1

   quickstart.md
   install.md
   config_file.md

Usage
-----

.. toctree::
   :maxdepth: 1

   integration.md
   config_items.md
   check.md
   notes_distribution.md  



----



API reference
--------------

.. toctree::
   :maxdepth: 3

   apidoc/integrator


# Installation

## At ESRF

At ESRF, the latest version of `integrator` is already installed,
and you can go directly to the [usage](quickstart.md) page.

## Custom/local installation

To install the stable version on your machine/environment:

```bash
pip install git+https://gitlab.esrf.fr/tomotools/integrator
```


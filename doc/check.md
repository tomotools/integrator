# Checking the integration status: `integrator-check`

Performing azimuthal integration on many datasets is usually long (tens of minutes to several hours).


You can check the status of azimuthal integration anytime by running

```bash
integrator-check integrator.conf
```

This will count the processed datasets and list the remaining ones. You only need to provide the configuration file as an argument.

There are optional parameters:
  - `--check_output=1`: check the validity (ex. data shape) of output files. Enabled by default.
  - `--list_missing_datasets=1`: list datasets not processed yet. Enabled by default.
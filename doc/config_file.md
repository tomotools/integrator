# Configuration file

The configuration file describes how the dataset should be processed. 

## Create a configuration file

A configuration file can be created using the following bootstrap command:
```bash
integrator-config
```
This will create a `integrator.conf` file with pre-filled values. Of course, you will still need to fill-in mandatory parameters.

More options are available, use `integrator-config --help` to see them.

**Don't copy a too-old configuration file**. It's always best to start from a fresh configuration file (`integrator-config`) and fill the values.
What you can do for example is:
```bash
integrator-config
meld integrator.conf /my/old/integrator.conf
# use arrows in meld to quickly update the lines of the new "integrator.conf"
```



## Using the configuration file

This file is the main parameter for all integrator CLI commands, for example:
```bash
integrate-slurm integrator.conf
```

```{seealso}
[Configuration file parameters](config_items)
```

## Notes on some parameters in the configuration file

### Dataset

In the `[dataset]` section, the parameter `location` is the path to a HDF5 file (`.h5`).
It is the path of the "Bliss master file" - this is a `.h5` file alongside the `scanXXXX` folders.

Example:

```ini
[dataset]
location = /data/visitor/blc15384/id15a/20240510/Align42keV/Align42keV_0001/Align42keV_0001.h5
```

**It can also be a list of files, with wildcars**.

Example:

```ini
[dataset]
location = /data/id15/inhouse4/ihma109/id15/ACL9011001b/ACL9011001b_00??/ACL9011001b_00??.h5, /data/id15/inhouse4/ihma109/id15/MJ1_1/MJ1_1_????/MJ1_1_????.h5
```

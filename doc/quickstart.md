# Quick start

If you are using this software from ESRF, first activate the integrator module:

```bash
source /scisoft/tomotools/integrator/activate
```

By default, the latest version is selected.

You can also select a version, eg. `source /scisoft/tomotools/integrator/activate 2024.1`


`integrator` is used in two steps:
  - Create/fill a [configuration file](config_file.md)
  - Run `integrate-slurm <config_file>` to distribute azimuthal integration over multiple CPUS/nodes.


## Create a configuration file

Create a [configuration file](config_file):

```bash
integrator-config
```

It will create a file named `integrator.conf` with pre-filled values.
More options are available, see `integrator-config --help`.

By default, most of the parameters are pre-filled.

The mandatory parameter to fill-in are:
  - `location` in `[dataset]` : path to the file(s) to process
  - `detector` in `[azimuthal integration]`: path to the detector definition
  - `mask_file` in `[azimuthal integration]`: path to the mask file
  - `poni_file` in `[azimuthal integration]`: path to the `.poni` file
  - `location` in `[output]`: path to the output integrated data directory





## Run the integration

Once you filled-in the configuration, to perform [distributed azimuthal integration](integration) on a dataset, 
just run

```bash
integrate-slurm integrator.conf
```

```{note}
The former `integrate-mp` command continues to work, but is now equivalent to `integrate-slurm`
```


More options are available, see `integrate-slurm --help`.  


#!/bin/bash
rm -rf apidoc
sphinx-apidoc --separate --ext-autodoc -o apidoc .. ../*/tests/*
rm -f apidoc/modules.rst
rm -f apidoc/setup.rst
sed -i '1s/.*/API reference/' apidoc/integrator.rst
sed -i '2s/.*/==============/' apidoc/integrator.rst

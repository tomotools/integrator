#!/usr/bin/env python
# coding: utf-8

from setuptools import setup, find_packages
from integrator import version

def setup_package():
    # doc_requires = []
    setup(
        name='integrator',
        author='Pierre Paleo',
        version=version,
        author_email = "pierre.paleo@esrf.fr",
        maintainer = "Pierre Paleo",
        maintainer_email = "pierre.paleo@esrf.fr",

        packages=find_packages(),

        package_data={
            "integrator.resources": [
                "*.dat",
            ],
        },
        include_package_data=True,

        install_requires = [
            'numpy > 1.9.0',
            'silx >= 0.15.0',
            'pyopencl',
            'h5py>=3.0.0',
            'hdf5plugin',
            'pyFAI',
            'tomoscan > 2.0.0',
            'nabu > 2022.2.2',
            'tqdm',
        ],
        doc_requires = [
            'sphinx',
            'myst-parser',
            'nbsphinx',
            #"karma-sphinx-theme"
            'sphinx_book_theme',
        ],
        long_description = """
        Integrator - distributed azimuthal integration
        """,

        entry_points = {
            'console_scripts': [
                "integrator-config=integrator.app.bootstrap:bootstrap",
                "integrate-mp=integrator.app.integrate_mp:integrate_mp_cli",
                "integrate-slurm=integrator.app.integrate_mp:integrate_mp_cli", # alias
                "integrate-mp-worker=integrator.app.integrate_mp_worker:integrate_mp_worker_cli",
                "integrator-check=integrator.app.check_datasets:check_datasets_cli",
                "integrator-pack=integrator.app.pack:pack_cli",
            ],
        },

        zip_safe=True
    )


if __name__ == "__main__":
    setup_package()

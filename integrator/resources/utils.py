import os

def get_folder_path(foldername=""):
    _file_dir = os.path.dirname(os.path.abspath(__file__))
    package_dir = _file_dir
    return os.path.join(package_dir, foldername)


def get_resource_file(filename):
    src_relpath = os.path.join("resources")
    return os.path.join(get_folder_path(), filename)


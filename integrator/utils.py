import logging
import os
from datetime import datetime
from contextlib import ContextDecorator
from subprocess import check_output, CalledProcessError
from ast import literal_eval
from threading import Timer
from pathlib import Path
from time import sleep
import posixpath
from .resources.utils import get_resource_file
import numpy as np
from psutil import cpu_count
from tomoscan.utils.hdf5 import get_data_sources
from nabu.io.writer import merge_hdf5_files, export_dict_to_h5
path = os.path

ai_units = {
    'q_a^-1': 'q_A^-1',
    'q_A^-1': 'q_A^-1',
    '2th_deg': '2th_deg',
    'r_mm': 'r_mm',
}

class PeriodicAction:
    def __init__(self, interval, function, args=None, kwargs=None):
        self.function = function
        self.interval = interval
        self._args = args
        self._kwargs = kwargs

    def _function_wrapper(self, *args, **kwargs):
        self.function(*args, **kwargs)
        self.start()

    def start(self):
        self.timer = Timer(self.interval, self._function_wrapper, args=self._args, kwargs=self._kwargs)
        self.timer.start()

    def stop(self):
        self.timer.cancel()

    cancel = stop


class PatchLogLevel(ContextDecorator):
    def __init__(self, prefix, loglevel):
        self.prefix = prefix
        self.new_loglevel = loglevel
        self.base_loglevels = {
            k: v.getEffectiveLevel()
            for k, v in logging.root.manager.loggerDict.items()
            if k.startswith(prefix) and isinstance(v, logging.Logger)
        }

    def __enter__(self):
        for logger_name in self.base_loglevels.keys():
            logging.getLogger(logger_name).setLevel(self.new_loglevel)

    def __exit__(self, exc_type, exc, exc_tb):
        for logger_name, old_log_level in self.base_loglevels.items():
            logging.getLogger(logger_name).setLevel(old_log_level)

"""
    with PatchLogLevel("nxtomomill", logging.CRITICAL):
        pass
"""


def make_azims(azim_min, azim_max, nb_slices):
    cake = (float(azim_min), float(azim_max))
    npieces = int(nb_slices)
    #-- calculate azim slices
    if npieces == 1:
        azims = [( cake[0], cake[1] )]
    #-- calculate azim slices if n.pieces > 1
    elif npieces > 1:
        vals = np.linspace(cake[0], cake[1], npieces + 1)
        azims = [(vals[i], vals[i + 1]) for i in range(npieces)]
    return np.array(azims)


def parse_poni_file(poni_file):
    conf = {}
    with open(poni_file) as opened_file:
        for line in opened_file:
            line = line.strip()
            if line.startswith("#") or (":" not in line):
                continue
            words = line.split(":", 1)
            key = words[0].strip().lower()
            try:
                value = words[1].strip()
            except Exception as error:
                print("Error %s with line: %s", error, line)
            conf[key] = value
    return conf


def check_poni_file_detector(poni_file, detector, force_replace=False):
    """
    Check a poni file so that the detector is replaced with another (user-provided) detector.
    This is useful when the detector file name within the poni file does not exist, or cannot be
    accessed.

    Parameters
    ----------
    poni_file: str
        Path to the poni file
    detector: str
        Name or path of the detector used to replace the one in the poni file
    force_replace: bool, optional
        Whether to use the provided detector even if the one in the poni file can be accessed
    """
    conf = parse_poni_file(poni_file)
    if "detector_config" not in conf:
        return conf
    det_conf = literal_eval(conf["detector_config"]) or {}
    det_fname = det_conf.get("filename", None)
    if force_replace or (det_fname is not None and not path.isfile(det_fname)):
        conf["detector_config"] = conf["detector_config"].replace(det_conf["filename"], detector)
    return conf



def get_h5_config_from_pyfai(pyfai_config, ai_config, dataset):
    h5_config = {
        "ai_config": str(ai_config),
        # CHECKME not sure about the order. But for now both are the same (from intrange code)
        "x_pixel_size": pyfai_config["pixel_size"][0],
        "y_pixel_size": pyfai_config["pixel_size"][1],
        ("x_pixel_size", "unit"): "mm",
        ("y_pixel_size", "unit"): "mm",
        "S-D distance": pyfai_config["S-D distance"],
        ("S-D distance", "unit"): "mm",
        "wavelength": pyfai_config["wavelength"],
        ("wavelength", "unit"): "angstrom",
        "pyfai_method": pyfai_config["pyfai_method"],
        "exposure time": dataset.exposure_time,
        ("exposure time", "unit"): "s",
    }
    return h5_config


def create_output_masterfile(
    files, output_file, ai_config, do_stack_mean, h5_config,
    entry="/", process_name = "azimuthal integration",
):
    local_files = files_relative_paths(files, output_file)
    azimuthal_ranges = make_azims(*ai_config.azimuthal_range)
    for azim_idx in range(len(azimuthal_ranges)):
        base_path = posixpath.join(entry, process_name, "results", str("azim_%02d" % (azim_idx + 1)))
        h5_paths = [posixpath.join(base_path, what) for what in ["intensities", "radial_positions"]]
        if ai_config.error_model is not None and ai_config.trim_method is None: # no error propagation when doing median/sigma-clip (?)
            h5_paths.append(posixpath.join(base_path, "error"))
        if do_stack_mean:
            h5_paths.append(posixpath.join(base_path, "averaged stack"))
        for h5_path in h5_paths:
            merge_hdf5_files(
                local_files, h5_path, output_file, process_name,
                output_entry=entry,
                config=None,
                base_dir=path.dirname(output_file),
                overwrite=True,
                # Not very elegant. 'data_name' expects something like "intensities",
                # but we pass something like 'azim_01/intensities'.
                # This works (h5py seems to create groups recursively) but might be brittle.
                data_name=posixpath.join(
                    str("azim_%02d" % (azim_idx + 1)), path.basename(h5_path)
                ),
            )
    # Save configuration
    h5_config["azimuthal_ranges"] = azimuthal_ranges
    h5_config["radial_range"] = ai_config.radial_range
    h5_config["result_files"] = local_files
    # h5_config["result_stacks"] = {}
    # for task_desc in self._tasks.values():
        # fname = path.basename(task_desc["result"])
        # h5_config["result_stacks"][fname] = task_desc["range"]
    export_dict_to_h5(
        h5_config, output_file, posixpath.join(entry, process_name, "configuration")
    )
    #


def browse_virtual_sources(dataset, logger):
    fname = dataset.dataset_hdf5_url.file_path()
    data_path = dataset.dataset_hdf5_url.data_path()
    virtual_sources = get_data_sources(fname, data_path)
    first_source = list(virtual_sources.keys())[0]
    if first_source == fname:
        # not a virtual dataset
        logger.error("Not a virtual dataset: %s" % fname)
        return None
    n_tries = 0
    vs = virtual_sources[first_source]
    dataset_virtual_sources = list(vs.keys())
    # Get the shape of first non-virtual dataset, instead of browsing everything
    while not isinstance(vs, str) and n_tries < 10:
        vs_fname = list(vs.keys())[0]
        vs = vs[vs_fname]
        n_tries += 1
    if n_tries >= 10:
        logger.error("Too many indirections in %s, stopping" % fname)
        vs_fname, vs = None, None
    dataset_first_vsource_fname = vs_fname
    dataset_first_vsource_path = vs
    return dataset_virtual_sources, dataset_first_vsource_fname, dataset_first_vsource_path


def retry_create_masterfile(n_retries, logger):
    def decorator(func):
        def wrapper(*args, **kwargs):
            files_list = args[0]
            success = False
            exception_to_raise = None
            for i in range(n_retries):
                if i > 0:
                    logger.info("Retrying to create master file (attempt %d/%d)" % (i, n_retries))
                try:
                    func(*args, **kwargs)
                    success = True
                except KeyError as exc:
                    success = False
                    exception_to_raise = exc
                    logger.error("Error when creating master file !")
                    sleep(2)
                    for fname in files_list:
                        Path(fname).touch()
                    sleep(2)
                if success:
                    break
            if not(success):
                raise exception_to_raise
        return wrapper
    return decorator


def retry_n_times(n_times, logger, func, *args, **kwargs):

    def log_message(msg):
        if logger is not None:
            logger.info(msg)
        else:
            print(msg)

    ret = "n/a"
    for i in range(n_times):
        try:
            ret = func(*args, **kwargs)
        except Exception as exc:
            log_message("==== Something went wrong, re-trying %d/%d" % (i + 1, n_times))
            ret = "error"
            ret_exc = exc
        if ret != "n/a" and ret != "error":
            break
        sleep(5)
    if ret == "error":
        raise ret_exc
    return ret


def walltime_to_seconds(walltime):
    """
    Converts a "walltime" formated string to a number of seconds.

    Parameters
    ----------
    walltime: str
        Walltime, in the form "Hours:Minutes:Seconds"


    Returns
    -------
    secs: int
        Number of seconds corresponding to walltime

    Examples
    ---------
    walltime_to_seconds("02:50:27") gives 10227

    """
    components = list(map(int, walltime.split(":")))
    if len(components) != 3:
        raise ValueError("Expected format H:M:S")
    return (np.array(components) * np.array([3600, 60, 1])).sum()




def whoami():
    ret = None
    try:
        ret = os.getlogin()
    except OSError: # can happen eg. in a SLURM reservation
        ret = os.environ.get("LOGNAME", None)
    return ret


def get_identifier(use_date=True):
    """
    Return a simple identifier in the form {whoami}_{date}
    """
    if use_date:
        res = "%s_%s" % (whoami(), datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
    else:
        res = "%s" % (whoami())
    return res



# TODO move to nabu
def partition_list(list_, n_sets):
    indices = np.arange(len(list_))
    sets_indices = np.array_split(indices, n_sets)
    results = []
    for set_indices in sets_indices:
        r = []
        for idx in set_indices:
            r.append(list_[idx])
        results.append(r)
    return results


# TODO move to nabu
def files_relative_paths(files, reference_file):
    """
    From a list of (absolute) files paths, return a list of relative paths.
    Each path is relative to 'reference_file'.
    """
    reference_dir = path.dirname(reference_file)
    local_files = []
    for file in files:
        relpath = path.relpath(file, start=reference_dir)
        # Sanitize paths like ".." or "../stuff"
        if relpath.startswith(".."):
            relpath = relpath[2:]
        if relpath.startswith("../"):
            relpath = relpath[3:]
        local_files.append(relpath)
    return local_files


def get_worker_threads_affinity(n_workers, n_threads_per_worker):
    """
    Given a number of workers (processes) and number of threads per worker,
    returns the optimal affinity list for each worker.

    Note that it likely won't work properly if the current host is used through
    a task scheduler(eg. SLURM). In this case, one must ensure that at least
    one full socket is allocated.
    """
    threads_per_core = cpu_count(logical=True) // cpu_count(logical=False)
    n_available_threads = len(os.sched_getaffinity(0))
    physical_cores = sorted(os.sched_getaffinity(0))[::threads_per_core]

    workers_phys_cores = partition_list(physical_cores, n_workers)
    if len(workers_phys_cores[0]) >= n_threads_per_worker:
        return [wpc[:n_threads_per_worker] for wpc in workers_phys_cores]

    wpc = np.array(workers_phys_cores)
    wt = wpc
    while wt.size < n_workers * n_threads_per_worker:
        wt = np.hstack([wt, wpc+1])
        wpc += 1
    return list(map(sorted, wt.tolist()))


# TODO move to nabu
def get_opencl_devices(device_type="gpu", excluded_platform=None):
    # Do it here, otherwise contexts are sometimes created on all platforms,
    # which will make further forks()/spawn() crash
    from pyopencl import get_platforms as cl_get_platforms, device_type as cl_device_type
    #
    dev_type = {
        "cpu": cl_device_type.CPU,
        "gpu": cl_device_type.GPU
    }[device_type.lower()]
    excluded_platform = excluded_platform or []
    tuples = []
    for i, platform in enumerate(cl_get_platforms()):
        if platform.name in excluded_platform:
            continue
        for j, device in enumerate(platform.get_devices()):
            if device.type == dev_type:
                tuples.append((i, j))
    return tuples


# TODO move to nabu (?)
class _Default_format(dict):
    """
    https://docs.python.org/3/library/stdtypes.html
    """
    def __missing__(self, key):
        return key

def safe_format(str_, **kwargs):
    """
    Alternative to str.format(), but does not throw a KeyError when fields are missing.
    """
    return str_.format_map(_Default_format(**kwargs))



def exec_shell_command(cmd):
    try:
        res = check_output(cmd.split())
        return res
    except RuntimeError as rerr:
        print("Error: %s" % rerr)
        return None
    except OSError as oerr:
        cmd0 = cmd.split(" ")[0]
        print("Something went wrong when executing %s. Please verify that the command %s is available" % (cmd, cmd0))
        print("Error: %s" % oerr)
        return None
    except CalledProcessError as err:
        print("Error: %s " %err)
        return None


def get_number_of_available_gpus(on_error="ignore"):
    """
    Get the number of available GPUs.

    Parameters
    ----------
    on_error: str, optional
        What to do on error. Can be "ignore", "print" or "raise".
    """
    # The current implementation is just a parsing of the "nvidia-smi" command.
    # Yes, this is terrible, and pyopencl should be used instead.
    # But in the current state of things:
    #   - pyopencl.get_platforms() sometimes (depending on the machine/setup) creates contexts
    #     all over the place, which will mess up further attemps to pick a device or simply fork/spawn.
    #   - We don't use GPUs other than Nvidia ones
    #
    # An equivalently-ugly but slightly better alternative would be to parse the output of
    # "clinfo", but (1) this command is not always available, (2) much more tedious to parse

    def handle_error(msg):
        if on_error == "raise":
            raise ValueError(msg)
        elif on_error == "print":
            print(msg)

    r = exec_shell_command("nvidia-smi -L")
    if r is None:
        handle_error("Could not detect any GPU")
        return 0
    devs = [x for x in r.decode().split(os.linesep) if len(x) > 0]
    if len(devs) == 0 or devs[0] == "No devices found.":
        handle_error("Could not detect any GPU")
        return 0
    return len(devs)


def file_mode_to_octal(file_mode, default=0o775):
    """
    Convert a file mode provided as a "regular int" (base 10) to an octal number of os.chmod()
    """
    if file_mode is None:
        return default
    try:
        res = int(str(file_mode), base=8)
    except ValueError: # already in octal ?
        res = file_mode
    return res


#
# Get additional metadata.
# Code from Stefano Checchia in hdintrange 0.12
#
from nabu.resources.logger import LoggerOrPrint
from h5py import File as H5pyFile

def get_additional_metadata(scan_number, h5abspath, axes, logger=None):
    """
    scankey: e.g. "{paths['sample']}_{paths['datasetshort']}_{paths['scan']}"
    h5abspath: e.g. /data/visitor/ch6066/id15/sample1/sample1_dataset4/sample1_dataset4.h5
    axes: e.g. [hry, hrz, temperature, potential, tfy]
    """
    out = {}
    logger = LoggerOrPrint(logger)
    logger.debug(f'Searching {h5abspath} for motor positions')
    categos = ['measurement','instrument/positioners','instrument/fscan_parameters']
    with H5pyFile(h5abspath, 'r') as hf:
        scan_entries = [k for k in hf if k.split('.')[0]==scan_number]   #e.g. ['6.1','6.2']
        try:
            out['exposure time'] = hf[f'{scan_entries[0]}/instrument/pilatus/acq_parameters/acq_expo_time'][()]
        except KeyError:
            out['exposure time'] = 1e-8
        for tgt in axes:
            has_values = 0
            for cat in categos:
                for sn in scan_entries:
                    logger.debug(f'Looking for {tgt} in {sn}/{cat}')
                    try:
                        if sn[-1] == '2' and tgt not in ['epoch','fpico2','fpico3']:
                            array = hf[f'{sn}/{cat}/{tgt}'][()]
                            timebase2 = hf[f'{sn}/measurement/epoch'][()]
                            timebase1 = hf[f'{sn.replace(".2",".1")}/measurement/epoch_trig'][()]
                            arrayinterp = np.interp(timebase1, xp=timebase2, fp=array)
                            out[tgt] = np.round(arrayinterp, 6).astype(np.float64)
                        else:
                            out[tgt] = np.round(hf[f'{sn}/{cat}/{tgt}'][()], 6).astype(np.float64)
                        logger.debug(f'Found {sn}/{cat}/{tgt}')
                        has_values = 1
                        break   # break sn
                    except KeyError as err:
                        logger.debug(f'{err}')
                        pass
                    except Exception as err:
                        logger.debug(f'{err}')
                else:   # this is necessary (tested)
                    continue   # this is necessary (tested)
                break   # break cat   # this is necessary (tested)
            else:  # this is not necessary
                continue   # this is not necessary
            if has_values == 0:
                logger.warning(f'Not found:{m}')
    return out


#
# Detector attenuation caused by non-zero thickness
# Code from Stefano Checchia in hdintrange 0.13
from scipy import constants
def make_detector_atten(wavelength_m, tth_rad, d_cm=0.1, logger=None):
    """
    econst (keV/A)
    X = 2theta_rad, 1D or 2D
    mu = linear attenuation coef (1/cm)
    d = sensor thickness (cm)
    K = exp((-mu*d)/cos(X))
    intens_final = intens_ini / K
    """

    ene_ev = (constants.h * constants.c / wavelength_m) / constants.e
    cdtefile = get_resource_file("cdte_mu_cm.dat")
    logger = LoggerOrPrint(logger)

    try:
        spl_ene, spl_att = np.loadtxt(cdtefile, unpack=True, usecols=(0,1), comments='#')
        mu = spl_att[np.argmin(abs(spl_ene-ene_ev))]
        logger.debug(f'Found {cdtefile}')

    except OSError as err:
        xcom_ene = 1e3 * np.array([5,6,8,10,15,20,26.71,26.71,30,31.81,31.81,40,
                                   50,60,70,80,90,100,110,120,130,140])
        # mass attenuation coefficients (cm2/g) for CdTe from XCOM database
        xcom_att = np.array([8.392e+02, 5.286e+02, 2.492e+02, 1.381e+02, 4.656e+01, 2.144e+01,
                             9.832e+00, 2.943e+01, 2.182e+01, 1.874e+01, 3.494e+01, 1.930e+01,
                             1.067e+01, 6.542e+00, 4.321e+00, 3.019e+00, 2.206e+00, 1.671e+00,
                             1.305e+00, 1.046e+00, 8.566e-01, 7.152e-01])
        sensor_density = 5.85
        spl = np.interp(ene_ev, xcom_ene, np.log10(xcom_att))
        mu = 10**spl * sensor_density
        logger.warning(f'{err} Recalculating value for {np.round(ene_ev):.0f} eV')

    logger.debug(f'CdTe LAC at {np.round(ene_ev):.0f} eV = {mu:.1f} / cm')
    K = (1-np.exp(-mu*d_cm/np.cos(tth_rad))) / (1-np.exp(-mu*d_cm))

    return K
#
# Templates for datasets paths
#

datasets_path_format = "{basename}/{proposal}/{beamline}/{subfolders}/{sample}/{dataset}/{scan}/{files}"



#
# Templates for output paths
#

output_id15a = "{basename}/{sample}/{dataset}/azint_{sample}_{dataset_num}_scan{scan_num}_{detector_name}.h5"


available_layouts = {
    "id15a": {
        "dataset": datasets_path_format,
        "output": output_id15a,
    },
    "id11": {
        "dataset": datasets_path_format,
        "output": output_id15a, # TODO
    },
}

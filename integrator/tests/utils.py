import os
from silx.resources import ExternalResources

utilstest = ExternalResources(
    project="integrator",
    url_base="http://www.silx.org/pub/nabu/data/integrator",
    env_key="INTEGRATOR_DATA",
    timeout=60
)

def get_test_data_path(*dataset_path):
    """
    Get a dataset file from silx.org/pub/nabu/data
    dataset_args is a list describing a nested folder structures, ex.
    ["path", "to", "my", "dataset.h5"]
    """
    dataset_relpath = os.path.join(*dataset_path)
    dataset_downloaded_path = utilstest.getfile(dataset_relpath)
    return dataset_downloaded_path
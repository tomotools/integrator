import os
from nabu.app.utils import parse_params_values
from .utils import get_distributed_integrator_params
from ..hdf5 import repack_output

PackConfig = {
    "config_file": {
        "help": "Path to the configuration file.",
        "default": "",
        "mandatory": True,
    },
}



def pack_cli():

    args = parse_params_values(
        PackConfig,
        parser_description="Check the progression of datasets integration"
    )
    conf_file = args["config_file"]

    _, _, _, datasets, output_files = get_distributed_integrator_params(
        conf_file,
        overwritten_params={"output": {"existing_output": "pass"}},
        parsing_error_handling="pass"
    )
    if datasets[0] is None or output_files[0] is None:
        print("Error: dataset location and output location have to be provided")
        exit(0)

    _, _, _, datasets, output_files = get_distributed_integrator_params(
        conf_file,
        print_already_processed=False,
        ignore_already_processed=False,
        parsing_error_handling="pass",
    )

    for dataset, output_file in zip(datasets, output_files):
        repack_output(output_file, dataset.detector_name, dataset.entry)


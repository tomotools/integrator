from multiprocessing import Process
from os import path, getcwd, mkdir, chmod
from pprint import pformat
from nabu.app.utils import parse_params_values
from nabu.resources.logger import Logger
from nabu.resources.utils import extract_parameters
from ..slurm import SlurmResourcesConfiguration, submit_bash_script
from ..processconfig import ProcessingConfig
from ..utils import get_identifier
from .utils import get_distributed_integrator_params
from ..monitoring import monitor_scans
from ..writer import watch_scans_and_create_output_files
from .. import version as integrator_version

IntegrateMPConfig = {
    "config_file": {
        "help": "Path to the configuration file.",
        "default": "",
        "mandatory": True,
    },
}

task_scheduler_submission_resources = {
    "esrf_p9gpu_half": "--partition p9gpu --gres=gpu:1 -c 64 --cores-per-socket 16 --time=01:00:00 --mem=200G",
    "esrf_p9gpu_full": "--partition p9gpu --gres=gpu:2 -c 128 --cores-per-socket 16 --time=01:00:00 --mem=400G",
}

def integrate_mp_cli():

    args = parse_params_values(
        IntegrateMPConfig,
        parser_description="Distribute azimuthal integration on one powerful machine"
    )
    conf_file = args["config_file"]

    conf, ai_config, comp_resources, datasets, output_files = get_distributed_integrator_params(
        conf_file,
    )
    if len(datasets) == 0:
        print("No dataset to integrate, exiting now.")
        exit(1)
    if datasets[0] is None or output_files[0] is None:
        print("Error: dataset location and output location have to be provided")
        exit(0)

    # Folders for log files and workspace
    logdir = path.join(path.relpath(getcwd()), "logs")
    workspace_dir = conf["computations distribution"]["workspace_path"] or path.join(path.relpath(getcwd()), ".integrator_workspace")
    for dir_ in [logdir, workspace_dir]:
        if not path.isdir(dir_):
            mkdir(dir_)
            chmod(dir_, 0o777)

    logger = Logger(
        "multiproc_integration",
        level=conf["pipeline"]["verbosity"],
        logfile=path.join(logdir, "multiproc_integration.log")
    )

    logger.info("Will process the following datasets:")
    logger.info(pformat(datasets))

    comp_cfg = conf["computations distribution"]
    partition_name = comp_cfg["partition"]

    # (TODO) Multi-process AI on the local machine
    if partition_name == "local":
        logger.info("Using the local machine to distribute computations")
        raise NotImplementedError()

    # ---

    # Build the tasks distribution
    process_config = ProcessingConfig(
        ai_config, comp_resources, datasets, output_files, workspace_directory=workspace_dir
    )
    workers_tasks, scans_info = process_config.build_tasks_distribution()
    workspace_file = process_config.dump_tasks_to_disk(workers_tasks)

    # Get the python environment to use on workers
    py_envs = extract_parameters(comp_cfg["python_executables"])
    python_env = py_envs[partition_name].replace(
        path.join("bin", "python"),
        path.join("bin", "activate")
    )
    python_env = python_env.format(current_version=integrator_version)
    logger.debug("Workers will be using python environment: %s" % python_env)

    # Distribute the work on SLURM
    _, conffile_name = path.split(path.abspath(conf_file))
    script_fname = path.join(
        workspace_dir,
        "%s_%s_integrate_mp.sh" % (path.splitext(conffile_name)[0], get_identifier(use_date=False))
    )
    generate_bash_script(python_env, workspace_file, script_fname)
    # TODO more flexible
    # "cores-per-socket >= 16" should prevent from getting old nodes
    cores = 32 if partition_name != "p9gpu" else 64
    slurm_resources = SlurmResourcesConfiguration(
        partition=partition_name,
        time=comp_resources.walltime,
        memory=comp_resources.memory,
        cpus_per_task=cores,
        gres="gpu:1",
        cores_per_socket=16
    )
    ret = submit_bash_script(
        slurm_resources, script_fname, logdir, workspace_dir,
        slurm_command_args=[str(v) for v in range(comp_resources.n_multi_worker)]
    )
    for r in ret:
        if isinstance(r, bytes):
            print(r.decode())


    #
    # Monitor integration progress
    #

    def do_in_process(func, *args, **kwargs):
        p = Process(target=func, args=args, kwargs=kwargs)
        p.start()
        return p

    logger_for_out_files_creation = Logger(
        "out_files_creation",
        level=conf["pipeline"]["verbosity"],
        logfile=path.join(logdir, "out_files_creation.log"),
        console=False,
    )
    do_in_process(
        watch_scans_and_create_output_files,
        scans_info,
        ai_config,
        conf["dataset"]["layout"],
        period=1,
        logger=logger_for_out_files_creation,
        extra_metadata={"config_file": conf_file, "configuration": conf},
        repack_output=conf["output"]["repack_output"],
        repack_output_options={
            "python_env": python_env,
            "slurm_resources": slurm_resources,
            "log_dir": logdir,
            "workspace_dir": workspace_dir,
        }
    )

    monitor_scans(scans_info, period=1, n_threads=4)



def generate_bash_script(python_environment, workspace_file, script_path):
    content = """#!/bin/bash -l
source {python_environment}

integrate-mp-worker {workspace_file} $1
""".format(
    python_environment=python_environment, workspace_file=workspace_file
)

    with open(script_path, "w") as fdesc:
        fdesc.write(content)

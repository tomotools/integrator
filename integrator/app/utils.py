import os
from pathlib import Path as pathlib_Path
from glob import glob
from tomoscan.esrf.scan.fscan import FscanDataset, list_datasets
from nabu.resources.utils import extract_parameters
from nabu.pipeline.config import overwrite_config, parse_nabu_config_file as parse_config_file, validate_config

from ..utils import safe_format
from ..integrator import AIConfiguration
from ..hdf5 import is_same_config
from ..utils import file_mode_to_octal
from .integrator_config import integrator_config, renamed_keys
from ..utils import get_additional_metadata
from ..multiproc_integration import ComputationResources
path = os.path


def get_computations_config(conf):
    """
    Converts a dictionary to a ClusterConfiguration object
    """

    if conf.get("n_workers", None) is None:
        if conf.get("computations distribution", None) is None:
            raise ValueError(
                "Passed parameter does not look like a valid configuration dictionary. Please use a dictionary built from the configuration file."
            )
        else:
            conf = conf["computations distribution"]


    # Get python executable to use on workers
    py_exes_list = conf["python_executables"].strip()
    if py_exes_list != "":
        py_exes_dict = extract_parameters(py_exes_list)
        partition = conf["partition"]
        if partition in py_exes_dict:
            py_exe = py_exes_dict[partition].strip()
        else:
            print("Could not find a python executable for partition '%s'" % partition)
    #
    computation_resources = ComputationResources(
        n_multi_worker=conf["n_workers"],
        n_workers_per_multiworker=conf["ai_engines_per_worker"],
        n_threads_per_worker=conf["cores_per_worker"],
        memory=conf["memory_per_worker"],
        walltime=conf["time"],
        queue=conf["partition"],
    )
    return computation_resources


def get_ai_config(conf):
    """
    Converts a dictionary to a AIConfiguration object
    """
    ai_conf_dict = conf["azimuthal integration"]
    ai_config = AIConfiguration(
        n_pts=ai_conf_dict["n_pts"],
        unit=ai_conf_dict["unit"],
        polarization_factor=ai_conf_dict["polarization_factor"],
        correct_solid_angle=bool(ai_conf_dict["correct_solid_angle"]),
        detector=ai_conf_dict["detector"],
        poni_file=ai_conf_dict["poni_file"],
        mask_file=ai_conf_dict["mask_file"],
        flatfield_file=ai_conf_dict["flatfield_file"],
        dark_file=ai_conf_dict["dark_file"],
        error_model=ai_conf_dict["error_model"],
        azimuthal_range=ai_conf_dict["azimuthal_range"],
        radial_range=ai_conf_dict["radial_range"],
        ai_method=ai_conf_dict["ai_method"],
        trim_method=ai_conf_dict["trim_method"],
        trim_n_pts=ai_conf_dict["trim_n_pts"],
        trim_bounds=ai_conf_dict["trim_bounds"],
        pixel_splitting=ai_conf_dict["pixel_splitting"],
        average_xy=ai_conf_dict["average_xy"],
        correct_detector_attenuation=ai_conf_dict["correct_detector_attenuation"],
    )
    if ai_config.azimuthal_range[-1] > 1:
        if ai_config.trim_method is not None:
            raise ValueError(
                "Multi-slice integration cannot be used with a trim method. Please either disable 'trim_method' or set the last number of 'azimuthal_range' to 1."
            )
        # if ai_conf_dict["average_xy"] is not None:
            # Current limitation
            # raise ValueError("Cannot use multiple azimuthal slices with average_xy=True")
    if ai_config.trim_method == "sigma_clip" and ai_config.pixel_splitting != "no":
        print("Combining sigma-clip and pixel splitting is not recommended. Disabling pixel splitting !")
        ai_config.pixel_splitting = "no"

    return ai_config


def browse_datasets_wildcard(wildcards, entry=None, detector_name=None, errors="print", extra_metadata_fields=[]):
    """
    Browse folders and look for datasets.
    This assumes that the datasets are stored in this layout:
    /path/to/samplename_num/samplename_num.h5

    Parameters
    ----------
    wildcards: str or list of str
        Wildcard or list of wildcards,
    """

    def handle_error(msg):
        if errors == "print":
            print(msg)
        elif errors == "raise":
            raise ValueError(msg)
        else:
            pass

    if isinstance(wildcards, str):
        wildcards = [wildcards]
    datasets = []
    for wildcard in wildcards:
        wildcard = wildcard.strip()
        fl = glob(wildcard)
        fl.sort()
        for fname in fl:
            if not path.isfile(fname):
                handle_error("No such file: %s" % fname)
            try:
                file_entries = list_datasets(fname) # can fail if Bliss file is broken
            except RuntimeError as exc:
                handle_error("Error with file %s: %s - ignoring this dataset!" % (fname, str(exc)))
                continue
            entries = [entry] if entry is not None else file_entries
            for d_entry in entries:
                try:
                    dataset = FscanDataset(fname, entry=d_entry, detector_name=detector_name, on_missing_metadata="ignore")
                    # Get extra metadata
                    scan_num = dataset.entry.split(".")[0]
                    new_metadata = get_additional_metadata(scan_num, dataset.fname, extra_metadata_fields, logger=None)
                    dataset.metadata.update(new_metadata)
                    #
                    datasets.append(dataset)
                except (ValueError, NameError, KeyError, IndexError, OSError) as exc:
                    handle_error("Error with file %s: %s - ignoring this dataset!" % (fname, str(exc)))
    return datasets


def get_output_folders(
    datasets,
    output_prefix,
    name_template="azint_{dataset}_{scan_num}_{detector}.h5",
    create_dirs=False,
    mode=None
):
    """
    From a list of datasets, returns a list of output paths corresponding to each file name.

    Parameters
    ----------
    datasets: list of HDF5Dataset
        List HDF5Dataset objects. The file path is expected to be in the ID15A layout "proposal/sample/dataset/scan(s)"
        example: /data/id15/inhouse6/ihxxx/id15/sampletest/sampletest_0008/sampletest_0008.h5 (possibly several scans therein)
    output_prefix: str
        Common path where output files will be saved
    name_template: str
        Template name. For example azint_{dataset}_{scan}_{detector}.h5 for azint_ACL9011001b_0009_scan0001_pilatus.h5
    create_dirs: bool, optional
        Whether to create output directories. Default is False.

    Returns
    -------
    output_files: list of str
        List of output files corresponding to each dataset
    datasets_info: dict
        Nested dictionary with the information on samples/datasets/scans
    """
    output_files = []
    datasets_info = {}
    filemode = file_mode_to_octal(mode)

    def _ensure_can_write(directory, filemode):
        try:
            os.chmod(directory, filemode)
        except PermissionError:
            print("Warning: cannot chmod %s to %s" % (directory, filemode))


    for dataset in datasets:
        # (/path/proposal/sample/dataset/scans, scans.h5)
        dirname, fname = path.split(dataset.fname)
        # assert path.basename(dirname) == path.splitext(fname)[0] # sampletest_0008/sampletest_0008.h5
        dataset_name = path.basename(dirname)
        sample_name = path.basename(path.dirname(dirname))
        scan_num = int(dataset.entry.split(".")[0])

        datasets_info.setdefault(sample_name, {})
        datasets_info[sample_name].setdefault(dataset_name, [])
        datasets_info[sample_name][dataset_name].append(dataset.entry) # dataset.entry.split(".")[0]

        file_name = safe_format(
            name_template,
            dataset=path.splitext(path.basename(dataset.fname))[0],
            scan_num="scan%04d" % scan_num,
            detector=dataset.detector_name,
        )

        out_file = path.join(
            output_prefix,
            sample_name,
            dataset_name,
            file_name,
        )
        output_files.append(out_file)
        if create_dirs:
            # os.makedirs(..., exist_ok=True) breaks sometimes. The following seems more robust
            pathlib_Path(path.dirname(out_file)).mkdir(parents=True, exist_ok=True)
        #
        # pathlib.Path.mkdir() combines with the umask, but it seems that os.umask() does not work with some network filesystems
        _ensure_can_write(path.dirname(out_file), filemode)
    # Finally, change filemode of upper dir if not already done
    _ensure_can_write(output_prefix, filemode)

    return output_files, datasets_info



# This function might be moved elsewhere in the future
def get_distributed_integrator_params(
    conf_file, overwritten_params=None, conf_dict=None,
    print_already_processed=True, ignore_already_processed=True, parsing_error_handling="print",
):
    """
    Parse and validate a configuration file, return several data structures detailed below.

    Parameters
    ----------
    conf_file: str
        Path to the configuration file
    overwritten_params: dict, optional
        Dictionary of parameters overwriting some options of the configuration file
    conf_dict: dict, optional
        Configuration dict to use instead of first parameter 'conf_file'.
    print_already_processed: bool, optional
        Print datasets already processed (if existing_output = skip)
    ignore_already_processed: bool, optional
        Remove already-processed datasets from the results list. Default is True
    parsing_error_handling: str, optional
        How to handle datasets parsing errors. Can be "print", "raise" or "pass".
        Default is "print".


    Returns
    -------
    conf: dict
        Nested dictionary extracted from the configuration file
    ai_config: AIConfiguration
        Data structure with the azimuthal integration configuration
    comp_resources: ComputationResources
        Data structure with the computation resources
    datasets: list of Dataset objects
        List where each item is a data structure with information on the dataset to process
    output_files: list of str
        List where each item is the path to the output file, either a .nx file or a directory
    """

    # Parse/validate configuration file
    if conf_dict is None:
        conf_dict = parse_config_file(conf_file)
        conffile_mtime = os.path.getmtime(conf_file)
    conf = validate_config(
        conf_dict,
        integrator_config,
        renamed_keys,
    )
    overwritten_params = overwritten_params or {}
    conf = overwrite_config(conf, overwritten_params)

    # Get AI configuration
    ai_config = get_ai_config(conf)

    # Get computation resources configuration
    comp_resources = get_computations_config(conf)

    #
    os.environ["OMP_NUM_THREADS"] = str(comp_resources.n_threads_per_worker)
    #

    # Get detector name.
    # 'detector' is usually the path to the detector specification, but in this case
    # we still miss the detector name (it's not in the detector description)
    detector_spec = conf["azimuthal integration"]["detector"].strip()
    detector_name = conf["azimuthal integration"]["detector_name"].strip()
    if not path.isfile(detector_spec):
        if detector_name != "" and detector_spec != detector_name:
            raise ValueError(
                "Conflicting options 'detector' and 'detector_name'. Please provide either a detector path and a detector name ; or just a detector name."
            )

    # Get beamline-specific dataset layout
    dataset_layout = conf["dataset"]["layout"]
    detector_name = detector_name or FscanDataset._default_detector_name

    # Browse dataset
    dataset_fname = conf["dataset"]["location"]
    if dataset_fname is None:
        # No dataset provided
        dataset_list = [None]
    else:
        datasets = dataset_fname.split(",")
        dataset_list = browse_datasets_wildcard(
            datasets,
            entry=conf["dataset"]["hdf5_entry"],
            detector_name=detector_name,
            errors=parsing_error_handling,
        )

    # Output file
    output_file = conf["output"]["location"]
    if output_file is None:
        if dataset_list[0] is not None:
            raise ValueError("Output file/directory must be provided when dataset location is provided")
        output_file_list = [None]
    output_file_list, output_folders_layout = get_output_folders(
        dataset_list, output_file, create_dirs=True, mode=conf["output"]["file_mode"]
    )

    # Skip datasets that are already processed
    what_to_do_if_exists = conf["output"]["existing_output"]
    if what_to_do_if_exists in ["skip", "reprocess_if_conffile_more_recent"] and ignore_already_processed:

        def print_if_necessary(msg):
            if print_already_processed:
                print(msg)

        new_dataset_list = []
        new_output_file_list = []
        for dataset, output_file in zip(dataset_list, output_file_list):
            skip = False
            if path.isfile(output_file):
                if what_to_do_if_exists == "skip" and is_same_config(output_file, ai_config):
                    skip = True
                elif what_to_do_if_exists == "reprocess_if_conffile_more_recent" and os.path.getmtime(output_file) > conffile_mtime:
                    skip = True
                # # DEBUG
                # elif what_to_do_if_exists == "reprocess_if_conffile_more_recent" and os.path.getmtime(output_file) <= conffile_mtime:
                #     skip = False
                #     print("Conf file more recent than %s - will have to reprocess!" % output_file)
                #     from datetime import datetime
                #     print(datetime.fromtimestamp(conffile_mtime).isoformat())
                #     print(datetime.fromtimestamp(os.path.getmtime(output_file)).isoformat())
                # # ----
            if skip:
                print_if_necessary(str("Discarding dataset %s: already processed" % (str(dataset))))
            else:
                new_dataset_list.append(dataset)
                new_output_file_list.append(output_file)
        dataset_list = new_dataset_list
        output_file_list = new_output_file_list

        #
    return conf, ai_config, comp_resources, dataset_list, output_file_list


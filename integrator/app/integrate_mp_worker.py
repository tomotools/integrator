from nabu.app.utils import parse_params_values
from ..multiproc_integration import MultiprocIntegrator
from ..processconfig import load_work_config

IntegrateMPWorkerConfig = {
    "workspace_file": {
        "help": "Path to the 'workspace file'.",
        "default": "",
        "mandatory": True,
    },
    "worker_id": {
        "help": "ID of current worker.",
        "default": "",
        "mandatory": True,
        "type": int
    },
}


def integrate_mp_worker_cli():
    args = parse_params_values(
        IntegrateMPWorkerConfig,
        parser_description="You are not supposed to use this command - it is used by integrate-mp."
    )
    workspace_file = args["workspace_file"]
    worker_id = args["worker_id"]

    work_config = load_work_config(workspace_file)
    my_tasks = work_config["tasks"][worker_id]

    workers_inputs = [t[0] for t in my_tasks]
    workers_outputs = [t[1] for t in my_tasks]

    logger = None # TODO


    multi_integrator = MultiprocIntegrator(
        work_config["ai_config"],
        work_config["computations_config"],
        logger=logger,
        extra_options={"log_dir": "logs"},
    )

    multi_integrator.process_scans(workers_inputs, workers_outputs)
